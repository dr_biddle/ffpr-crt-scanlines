# FFPR CRT and Scanlines

CRT, Scanlines, Vibrance, and HDR filter presets intended for the Final Fantasy Pixel Remaster series of games

![FF6PR Before](https://cdn.discordapp.com/attachments/946166408872624148/946792691205828648/unknown.png)*Before*
![FF6PR After](https://cdn.discordapp.com/attachments/946166408872624148/946803608928792576/unknown.png)*After*

## Requirements

- [ReShade](https://reshade.me)
- [CRT Royale Filter for ReShade](https://github.com/akgunter/crt-royale-reshade)
- [RSRetroArch Repository for ReShade](https://github.com/Matsilagi/RSRetroArch)

## Settings

Ensure **CRT_Royale (crt-royale.fx)**, **Scanlinesfract (scanlines-fract.fx)**, **HDR (FakeHDR.fx)**, and **Vibrance (Vibrance.fx)** are enabled in ReShade.

![ReShade Settings](https://cdn.discordapp.com/attachments/946166408872624148/946807491625381888/unknown.png)*ReShade Settings*

## Support/Issues

If you have trouble, visit the VG Research & Modding discord at https://discord.gg/bSnpVBV
